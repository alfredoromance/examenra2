package starwars;

import java.util.Scanner;

public class starwars {

	final static int TAMANO = 6;
	final static char JEDI = 'J';
	final static char VADER = 'V';
	final static char SIDIOUS = 'S';
	final static char MAUL = 'M';
	final static char VACIO = '_';
	static boolean armadura;	
	
	public static void main(String[] args) {
		char[][] tablero = new char[TAMANO][TAMANO];
		Scanner input = new Scanner(System.in);
		char movimiento;
		armadura = true;
		
		// inicilizar tablero;
		inicializarTablero(tablero);
		//emplazar jugadores
		emplazarJugadores(tablero);
		
		do{
			//mostrar tablero
			mostrarTablero(tablero);
			//pedir movimiento
			movimiento = pedirMovimiento(input);
			//realizar movimiento del jedi
			jugadaJedi(tablero, movimiento);
			// realizar movimientos de enemigos
			  sithvencidos(tablero);
			jugadaSith(tablero);
			
		}while(!juegoTerminado(tablero));
		mostrarTablero(tablero);
		
		input.close();
	}

	private static void jugadaSith(char[][] tablero) {
		 movimientoSith(tablero, 'V');
		    movimientoSith(tablero, 'S');
		    movimientoSith(tablero, 'M');
		
	}
	
private static void movimientoSith(char[][] tablero, char movimiento) {
	int direccionmovimiento = 0;
    int movimientoI = 0;
    int movimientoJ = 0;
    for (int i = 0; i < tablero.length; i++) {
		for (int j = 0; j < tablero.length; j++) {
			//hago un if diriendole cuantos enemigos hay
			if (tablero[i][j]== movimiento) {
				do {//para que mueva hacia una direccion 
					direccionmovimiento =(int)(Math.random()*4);
					//lo mismo que el jedi
					switch (direccionmovimiento) {
					case 0:
						 movimientoI = i;
			              movimientoJ = j - 1;
						break;
					case 1:
						 movimientoI = i;
			              movimientoJ = j + 1;
						break;
					case 2:
						   movimientoI = i - 1;
				              movimientoJ = j;
						break;
					case 3:
						 movimientoI = i + 1;
			              movimientoJ = j;
						break;
					}
					
				} while (!ComprobarCoordendas (movimientoI, movimientoJ));
				 if (ComprobarCoordendas(movimientoI, movimientoJ)) {
				tablero[movimientoI][movimientoJ] = movimiento;
	            tablero[i][j] = VACIO;
				 }
			}
			
		}
	}
		
	}

	//este metodo solo tengo que recorer el array con un contador y decirle si llega a 0 temine unos u otros
	//dentro del bucle recorro el mapa diciendo cuantos jugadores hay en cada momento
	private static boolean juegoTerminado(char[][] tablero) {
		 int jedi = 1;
		    int contSith = 1;
		    for (int i = 0; i < tablero.length; i++) {
		      for (int j = 0; j < tablero[0].length; j++) {
		        if (tablero[i][j] == 'J') {
		          jedi++;
		        }
		         if ((tablero[i][j] == 'M') || (tablero[i][j] == 'V') || (tablero[i][j] == 'S')) {
		          contSith++;
		        }
		      }
		    }
		    
		    if (jedi == 1) {
		    	System.out.println();
		 System.out.println("****************************************");
		      System.out.println("         GAME OVER ");
		      System.out.println("   Los Sith te han vencido ");
		  System.out.println("****************************************");
		      return true;
		    }
		    if (contSith == 1) {
		    	System.out.println();
		      System.out.println("Enhorabuena los rebeldes vivir�n para otra batalla");
		      return true;
		    }
		    
		    return false;
	}

	private static void jugadaJedi(char[][] tablero, char movimiento) {
		//jugadajedi saco la posicion I y la posicion j 
		 int posicionI = coordenadaIJedi(tablero, 'J');
		    int posicionJ = coordenadaJJedi(tablero, 'J');
		    int movimientoI = posicionI;
		    int movimientoJ = posicionJ;
		    //creo el movimiento con switch
		    switch (movimiento) {
		    case 'a': 
		      movimientoI = posicionI;
		      movimientoJ = posicionJ - 1;
		      break;
		    
		    case 'd': 
		      movimientoI = posicionI;
		      movimientoJ = posicionJ + 1;
		      break;
		    
		    case 'w': 
		      movimientoI = posicionI - 1;
		      movimientoJ = posicionJ;
		      break;
		    
		    case 's': 
		      movimientoI = posicionI + 1;
		      movimientoJ = posicionJ;
		    
		    } //delimito el tablero para que no se salga luck
		    if (ComprobarCoordendas(movimientoI, movimientoJ)) {
			    
			     
			      tablero[posicionI][posicionJ] = VACIO;
			      tablero[movimientoI][movimientoJ] = JEDI;
			    }
		 
		  
					
				
			  } 
	



private static void sithvencidos(char[][] tablero) {
		// TODO Auto-generated method stub
	int contadorMaul =0;
	int contadorVader=0;
	int contadorSidious=0;
	 for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				if ((tablero[i][j] == 'M') ) {
					 contadorMaul++;
			        }
				if (tablero[i][j] == 'V') {
					contadorVader++;
				}
				if ((tablero[i][j] == 'S')) {
					contadorSidious++;
				}
					
				
			}
		}
	 System.out.println("Enemigos derotados:");
	 System.out.println("**********************************************");
	 if (contadorMaul ==0) {
		System.out.println("Darth Maul ha sido derrotado");
	}
	 if (contadorVader==0) {
			System.out.println("Darth Vader ha sido derrotado");
	}
	 if (contadorSidious==0) {
			System.out.println("Darth Sidious ha sido derrotado");
	}
	
	}

//delimito el tablero como tengo una matriz de 6 estaran entre i maximo 5 y j maximo 5
	private static boolean ComprobarCoordendas(int movimientoI, int movimientoJ) {
		   if ((movimientoI >= 0) && (movimientoI < 6) && (movimientoJ >= 0) && (movimientoJ < 6)) {
			      return true;
			    }
			    return false;
	}

	private static int coordenadaJJedi(char[][] tablero, char c) {
		  int coordenada = 0;
		    for (int i = 0; i < tablero.length; i++) {
		      for (int j = 0; j < tablero[0].length; j++) {
		        if (tablero[i][j] == c) {
		          return j;
		        }
		      }
		    }
		    return coordenada;
	}

	private static int coordenadaIJedi(char[][] tablero, char c) {
		int coordenada = 0;
	    for (int i = 0; i < tablero.length; i++) {
	      for (int j = 0; j < tablero[0].length; j++) {
	        if (tablero[i][j] == c) {
	          return i;
	        }
	      }
	    }
	    return coordenada;
	}

	private static char pedirMovimiento(Scanner input) {
		 char direccion = ' ';
		    do {
		      System.out.println("�Direcci�n de movimiento? -->a,d,w,s");
		      direccion = input.nextLine().toLowerCase().charAt(0);
		    } while ((direccion != 'a') && (direccion != 'w') && (direccion != 'd') && (direccion != 's'));
		    
		    return direccion;
	}

	private static void mostrarTablero(char[][] tablero) {
		for (int i = 0; i < tablero.length + 2; i++) {
			System.out.print("0 ");
		}
		System.out.println();
		
		for (int i = 0; i < tablero.length; i++) {
			System.out.print("0 ");
			for (int j = 0; j < tablero[i].length; j++) {
				System.out.print(tablero[i][j]+ " ");
			}
			System.out.println("0");
		}
		
		for (int i = 0; i < tablero.length + 2; i++) {
			System.out.print("0 ");
		}
		System.out.println();
		
	}

	private static void emplazarJugadores(char[][] tablero) {
		tablero[0][0] = JEDI;
		tablero[0][TAMANO-1] = SIDIOUS;
		tablero[TAMANO-1][0] = VADER;
		tablero[TAMANO-1][TAMANO-1] = MAUL;
	}

	private static void inicializarTablero(char[][] tablero) {
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				tablero[i][j] = VACIO;
			}
		}	
	}

}
